class UsersController < ApplicationController
	before_action :set_user, only: %w(show edit destroy)
	before_action :check_authorization, except: :home
	def new
		@user = User.new
	end	

	def create
		@user = User.new(user_params)
		respond_to do |format|
			if @user.save
				format.html { redirect_to users_path, notice: 'User was successfully created.' }
	      format.json { render :index, status: :created, location: @user }
			else
				format.html { render :new }
	      format.json { render json: @user.errors, status: :unprocessable_entity }
			end
		end
	end

	def index
		@users = User.where(type: nil)
	end

	def show; end
	def edit; end

	def update
		respond_to do |format|
			if current_user.update(user_params)
				bypass_sign_in(current_user)
				format.html { redirect_to users_path, notice: 'User was update created.' }
	      format.json { render :index, status: :created, location: current_user }
			else
				format.html { render :edit }
	      format.json { render json: current_user.errors, status: :unprocessable_entity }
			end
		end
	end

	def destroy
		if @user.destroy
			redirect_to users_path, notice: 'User was successfully deleted.'
		end
	end

	private 

	def check_authorization
		unless  current_user.admin?
			redirect_to home_users_path, alert: 'You are not authorized'
		end
	end

	def set_user
		@user = User.find(params[:id])
	end

	def user_params
		params.require(:user).permit(:email, :name, :password)
	end
end
