class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :name, presence: true

  ROLE = ['Admin', 'User']
	ROLE.each do |role|
		define_method "#{role.downcase}?" do
			type == role
		end
	end

end
  
