json.extract! application_user, :id, :created_at, :updated_at
json.url application_user_url(application_user, format: :json)
