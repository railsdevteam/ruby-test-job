Rails.application.routes.draw do
	root to: 'users#home'
  devise_for :users
  resources :users, except: :create do
    collection do
      get :home
    end
  end
  post 'create_user' => 'users#create', as: :create_user      
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
