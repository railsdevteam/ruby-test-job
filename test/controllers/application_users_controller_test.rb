require 'test_helper'

class ApplicationUsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @application_user = application_users(:one)
  end

  test "should get index" do
    get application_users_url
    assert_response :success
  end

  test "should get new" do
    get new_application_user_url
    assert_response :success
  end

  test "should create application_user" do
    assert_difference('ApplicationUser.count') do
      post application_users_url, params: { application_user: {  } }
    end

    assert_redirected_to application_user_url(ApplicationUser.last)
  end

  test "should show application_user" do
    get application_user_url(@application_user)
    assert_response :success
  end

  test "should get edit" do
    get edit_application_user_url(@application_user)
    assert_response :success
  end

  test "should update application_user" do
    patch application_user_url(@application_user), params: { application_user: {  } }
    assert_redirected_to application_user_url(@application_user)
  end

  test "should destroy application_user" do
    assert_difference('ApplicationUser.count', -1) do
      delete application_user_url(@application_user)
    end

    assert_redirected_to application_users_url
  end
end
